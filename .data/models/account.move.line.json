{
    "id": 205,
    "name": "account.move.line",
    "description": "Journal Item",
    "modelFields": {
        "narration": {
            "string": "Narration",
            "type": "text"
        },
        "display_name": {
            "string": "Display Name",
            "type": "char"
        },
        "credit_cash_basis": {
            "string": "Credit Cash Basis",
            "type": "monetary"
        },
        "create_date": {
            "string": "Created on",
            "type": "datetime"
        },
        "currency_id": {
            "string": "Currency",
            "type": "many2one",
            "help": "The optional other currency if it is a multi-currency entry."
        },
        "statement_id": {
            "string": "Statement",
            "type": "many2one",
            "help": "The bank statement used for bank reconciliation"
        },
        "balance_cash_basis": {
            "string": "Balance Cash Basis",
            "type": "monetary",
            "help": "Technical field holding the debit_cash_basis - credit_cash_basis in order to open meaningful graph views from reports"
        },
        "matched_credit_ids": {
            "string": "Matched Credit",
            "type": "one2many",
            "help": "Credit journal items that are matched with this journal item."
        },
        "full_reconcile_id": {
            "string": "Matching Number",
            "type": "many2one"
        },
        "id": {
            "string": "ID",
            "type": "integer"
        },
        "tax_exigible": {
            "string": "Appears in VAT report",
            "type": "boolean",
            "help": "Technical field used to mark a tax line as exigible in the vat report or not (only exigible journal items are displayed). By default all new journal items are directly exigible, but with the feature cash_basis on taxes, some will become exigible only when the payment is recorded."
        },
        "is_unaffected_earnings_line": {
            "string": "Is Unaffected Earnings Line",
            "type": "boolean",
            "help": "Tells whether or not this line belongs to an unaffected earnings account"
        },
        "user_type_id": {
            "string": "Type",
            "type": "many2one",
            "help": "Account Type is used for information purpose, to generate country-specific legal reports, and set the rules to close a fiscal year and generate opening entries."
        },
        "analytic_account_id": {
            "string": "Analytic Account",
            "type": "many2one"
        },
        "invoice_id": {
            "string": "Invoice",
            "type": "many2one"
        },
        "debit": {
            "string": "Debit",
            "type": "monetary"
        },
        "product_id": {
            "string": "Product",
            "type": "many2one"
        },
        "expense_id": {
            "string": "Expense",
            "type": "many2one",
            "help": "Expense where the move line come from"
        },
        "debit_cash_basis": {
            "string": "Debit Cash Basis",
            "type": "monetary"
        },
        "account_id": {
            "string": "Account",
            "type": "many2one"
        },
        "write_uid": {
            "string": "Last Updated by",
            "type": "many2one"
        },
        "write_date": {
            "string": "Last Updated on",
            "type": "datetime"
        },
        "tax_line_id": {
            "string": "Originator tax",
            "type": "many2one"
        },
        "date_maturity": {
            "string": "Due date",
            "type": "date",
            "help": "This field is used for payable and receivable journal entries. You can put the limit date for the payment of this line."
        },
        "activity_type": {
            "string": "Activity Type",
            "type": "selection"
        },
        "product_uom_id": {
            "string": "Unit of Measure",
            "type": "many2one"
        },
        "company_id": {
            "string": "Company",
            "type": "many2one"
        },
        "name": {
            "string": "Label",
            "type": "char"
        },
        "create_uid": {
            "string": "Created by",
            "type": "many2one"
        },
        "blocked": {
            "string": "No Follow-up",
            "type": "boolean",
            "help": "You can check this box to mark this journal item as a litigation with the associated partner"
        },
        "counterpart": {
            "string": "Counterpart",
            "type": "char",
            "help": "Compute the counter part accounts of this journal item for this journal entry. This can be needed in reports."
        },
        "journal_id": {
            "string": "Journal",
            "type": "many2one"
        },
        "analytic_line_ids": {
            "string": "Analytic lines",
            "type": "one2many"
        },
        "matched_debit_ids": {
            "string": "Matched Debit",
            "type": "one2many",
            "help": "Debit journal items that are matched with this journal item."
        },
        "amount_residual_currency": {
            "string": "Residual Amount in Currency",
            "type": "monetary",
            "help": "The residual amount on a journal item expressed in its currency (possibly not the company currency)."
        },
        "move_id": {
            "string": "Journal Entry",
            "type": "many2one",
            "help": "The move of this entry line."
        },
        "date": {
            "string": "Date",
            "type": "date"
        },
        "partner_id": {
            "string": "Partner",
            "type": "many2one"
        },
        "quantity": {
            "string": "Quantity",
            "type": "float",
            "help": "The optional quantity expressed by this line, eg: number of product sold. The quantity is not a legal requirement but is very useful for some reports."
        },
        "amount_currency": {
            "string": "Amount Currency",
            "type": "monetary",
            "help": "The amount expressed in an optional other currency if it is a multi-currency entry."
        },
        "analytic_tag_ids": {
            "string": "Analytic tags",
            "type": "many2many"
        },
        "tax_base_amount": {
            "string": "Base Amount",
            "type": "monetary"
        },
        "parent_state": {
            "string": "Parent State",
            "type": "char",
            "help": "State of the parent account.move"
        },
        "reconciled": {
            "string": "Reconciled",
            "type": "boolean"
        },
        "payment_id": {
            "string": "Originator Payment",
            "type": "many2one",
            "help": "Payment that created this entry"
        },
        "company_currency_id": {
            "string": "Company Currency",
            "type": "many2one",
            "help": "Utility field to express amount currency"
        },
        "branch_id": {
            "string": "Branch",
            "type": "many2one"
        },
        "statement_line_id": {
            "string": "Bank statement line reconciled with this entry",
            "type": "many2one"
        },
        "tax_ids": {
            "string": "Taxes",
            "type": "many2many"
        },
        "ref": {
            "string": "Reference",
            "type": "char"
        },
        "credit": {
            "string": "Credit",
            "type": "monetary"
        },
        "__last_update": {
            "string": "Last Modified on",
            "type": "datetime"
        },
        "balance": {
            "string": "Balance",
            "type": "monetary",
            "help": "Technical field holding the debit - credit in order to open meaningful graph views from reports"
        },
        "amount_residual": {
            "string": "Residual Amount",
            "type": "monetary",
            "help": "The residual amount on a journal item expressed in the company currency."
        }
    }
}