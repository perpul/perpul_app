{
    "id": 134,
    "name": "mail.mail",
    "description": "Outgoing Mails",
    "modelFields": {
        "delete_date": {
            "string": "Delete on.",
            "type": "date"
        },
        "display_name": {
            "string": "Display Name",
            "type": "char"
        },
        "create_date": {
            "string": "Created on",
            "type": "datetime"
        },
        "author_id": {
            "string": "Author",
            "type": "many2one",
            "help": "Author of the message. If not set, email_from may hold an email address that did not match any partner."
        },
        "model": {
            "string": "Related Document Model",
            "type": "char"
        },
        "partner_ids": {
            "string": "Recipients",
            "type": "many2many"
        },
        "website_published": {
            "string": "Published",
            "type": "boolean",
            "help": "Visible on the website as a comment"
        },
        "fetchmail_server_id": {
            "string": "Inbound Mail Server",
            "type": "many2one"
        },
        "message_type": {
            "string": "Type",
            "type": "selection",
            "help": "Message type: email for email message, notification for system message, comment for other messages such as user replies"
        },
        "state": {
            "string": "Status",
            "type": "selection"
        },
        "statistics_ids": {
            "string": "Statistics",
            "type": "one2many"
        },
        "id": {
            "string": "ID",
            "type": "integer"
        },
        "no_auto_thread": {
            "string": "No threading for answers",
            "type": "boolean",
            "help": "Answers do not go in the original document discussion thread. This has an impact on the generated message-id."
        },
        "write_uid": {
            "string": "Last Updated by",
            "type": "many2one"
        },
        "starred": {
            "string": "Starred",
            "type": "boolean",
            "help": "Current user has a starred notification linked to this message"
        },
        "mailing_id": {
            "string": "Mass Mailing",
            "type": "many2one"
        },
        "author_avatar": {
            "string": "Author's avatar",
            "type": "binary",
            "help": "Small-sized image of this contact. It is automatically resized as a 64x64px image, with aspect ratio preserved. Use this field anywhere a small image is required."
        },
        "write_date": {
            "string": "Last Updated on",
            "type": "datetime"
        },
        "attachment_ids": {
            "string": "Attachments",
            "type": "many2many",
            "help": "Attachments are linked to a document through model / res_id and to the message through this field."
        },
        "email_to": {
            "string": "To",
            "type": "text",
            "help": "Message recipients (emails)"
        },
        "notification_ids": {
            "string": "Notifications",
            "type": "one2many"
        },
        "recipient_ids": {
            "string": "To (Partners)",
            "type": "many2many"
        },
        "parent_id": {
            "string": "Parent Message",
            "type": "many2one",
            "help": "Initial thread message."
        },
        "body_html": {
            "string": "Rich-text Contents",
            "type": "text",
            "help": "Rich-text/HTML message"
        },
        "body": {
            "string": "Contents",
            "type": "html"
        },
        "rating_ids": {
            "string": "Related ratings",
            "type": "one2many"
        },
        "path": {
            "string": "Discussion Path",
            "type": "char",
            "help": "Used to display messages in a paragraph-based chatter using a unique path;"
        },
        "__last_update": {
            "string": "Last Modified on",
            "type": "datetime"
        },
        "mail_server_id": {
            "string": "Outgoing mail server",
            "type": "many2one"
        },
        "description": {
            "string": "Description",
            "type": "char",
            "help": "Message description: either the subject, or the beginning of the body"
        },
        "res_id": {
            "string": "Related Document ID",
            "type": "integer"
        },
        "auto_delete": {
            "string": "Auto Delete",
            "type": "boolean",
            "help": "Permanently delete this email after sending it, to save space"
        },
        "record_name": {
            "string": "Message Record Name",
            "type": "char",
            "help": "Name get of the related document."
        },
        "headers": {
            "string": "Headers",
            "type": "text"
        },
        "starred_partner_ids": {
            "string": "Favorited By",
            "type": "many2many"
        },
        "message_id": {
            "string": "Message-Id",
            "type": "char",
            "help": "Message unique identifier"
        },
        "references": {
            "string": "References",
            "type": "text",
            "help": "Message references, such as identifiers of previous messages"
        },
        "needaction_partner_ids": {
            "string": "Partners with Need Action",
            "type": "many2many"
        },
        "child_ids": {
            "string": "Child Messages",
            "type": "one2many"
        },
        "notification": {
            "string": "Is Notification",
            "type": "boolean",
            "help": "Mail has been created to notify people of an existing mail.message"
        },
        "subtype_id": {
            "string": "Subtype",
            "type": "many2one"
        },
        "date": {
            "string": "Date",
            "type": "datetime"
        },
        "create_uid": {
            "string": "Created by",
            "type": "many2one"
        },
        "reply_to": {
            "string": "Reply-To",
            "type": "char",
            "help": "Reply email address. Setting the reply_to bypasses the automatic thread creation."
        },
        "email_from": {
            "string": "From",
            "type": "char",
            "help": "Email address of the sender. This field is set when no matching partner is found and replaces the author_id field in the chatter."
        },
        "scheduled_date": {
            "string": "Scheduled Send Date",
            "type": "char",
            "help": "If set, the queue manager will send the email after the date. If not set, the email will be send as soon as possible."
        },
        "mail_activity_type_id": {
            "string": "Mail Activity Type",
            "type": "many2one"
        },
        "keep_days": {
            "string": "Keep days",
            "type": "integer",
            "help": "This value defines the no. of days the emails should be recorded in the system: \n -1 = Email will be deleted immediately once it is send \n greater than 0 = Email will be deleted after the no. of days are met."
        },
        "subject": {
            "string": "Subject",
            "type": "char"
        },
        "failure_reason": {
            "string": "Failure Reason",
            "type": "text",
            "help": "Failure reason. This is usually the exception thrown by the email server, stored to ease the debugging of mailing issues."
        },
        "mail_message_id": {
            "string": "Message",
            "type": "many2one"
        },
        "email_cc": {
            "string": "Cc",
            "type": "char",
            "help": "Carbon copy message recipients"
        },
        "channel_ids": {
            "string": "Channels",
            "type": "many2many"
        },
        "needaction": {
            "string": "Need Action",
            "type": "boolean",
            "help": "Need Action"
        },
        "rating_value": {
            "string": "Rating Value",
            "type": "float"
        }
    }
}