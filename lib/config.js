/*
 * Create and export configuration variables
 *
 */
var perpulServers = {};
// Container for all PerpulServers
perpulServers.staging = {
	  'url' : 'http://client1.perpul.co',
	  'port' : 7073,
	  'db' : 'client1.perpul.co',
	  'username' : 'admin',
	  'password' : 'admin'
};



// Determine which PerpulServer was passed as a command-line argument
var currentPerpulServer = typeof(process.env.NODE_ENV) == 'string' ? process.env.NODE_ENV.toLowerCase() : '';

// Check that the current PerpulServer is one of the PerpulServers above, if not default to staging
var perpulServer = typeof(perpulServers[currentPerpulServer]) == 'object' ? perpulServers[currentPerpulServer] : perpulServers.staging;


// Export the module
module.exports = perpulServer;
