var Perpul = require('../lib/index');
var perpul = new Perpul();
var perpulModel = {};
var inParams = [];

var perpulModel = {};

// count records
perpulModel.countRecords = function(queryObject){
    inParams.push(queryStringArray);
    var data = {
        'queryType' : 'search_count',
        'inParams' : inParams,
        'model' : model
    };
    perpulModel.executeQuery(data);
};

// create record
perpulModel.createRecord = function(modelObject){
    inParams.push(modelObject);
    var data = {
        'queryType' : 'create',
        'inParams' : inParams, 
        'model' : modelObject.model
    };
    perpulModel.executeQuery(data);
};

// delete record
perpulModel.deleteRecord = function(modelObject){
    inParams.push([modelObject.id]);
    var data = {
        'queryType' : 'unlink',
        'inParams' : inParams, 
        'model' : modelObject.name
    };
    perpulModel.executeQuery(data);
};


// list records
perpulModel.listRecords = function(queryObject){
    inParams.push([queryString]);
    inParams.push(10); //offset
    inParams.push(5);  //limit
    var data = {
        'queryType' : 'search',
        'inParams' : inParams,
        'model' : model
    };
    perpulModel.executeQuery(data);
};

// list record fields
perpulModel.listRecordFields = function(model){
    inParams.push([]);
    inParams.push([]);
    inParams.push([]);
    inParams.push(['string', 'help', 'type']); 
    var data = {
        'queryType' : 'fields_get',
        'inParams' : inParams,
        'model' : model
    };
    perpulModel.executeQuery(data); 
};


// search and read 
perpulModel.searchRead = function(queryObject){
    inParams.push(queryStringArray);
    inParams.push(['name', 'country_id', 'comment']); //fields
    inParams.push(0); //offset
    inParams.push(5); //limit
    var data = {
        'queryType' : 'search_read',
        'inParams' : inParams,
        'model' : model
    };
    perpulModel.executeQuery(data); 
};

// update record
perpulModel.updateRecords = function(modelObject){
    inParams.push([modelObject.id]); //id to update
    inParams.push(modelObject);
    var data = {
        'queryType' : 'write',
        'inParams' : inParams,
        'model' : model
    };
    perpulModel.executeQuery(data);
};

perpulModel.executeQuery = function(data){
    var model = data.model;
    var queryType = data.queryType;
    var params = [];
    params.push(data.inParams);
    perpul.connect(function (err) {
        if (err) { return console.log(err); }       
        perpul.execute_kw(model, queryType, params, function (err, value) {
            if (err) { return console.log(err); }
            // do something with value
        });
    });
};

module.exports = perpulModel;